package edu.towson.cosc435.Achianga.todos

import android.app.Activity
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import com.google.gson.Gson
import edu.towson.cosc435.Achianga.todos.models.TodoItem
import kotlinx.android.synthetic.main.activity_new_to_do.*

class NewToDoActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_to_do)

        save_btn.setOnClickListener(this)
    }
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onClick(view: View){
        when(view?.id){
            R.id.save_btn ->{
                val intent = Intent()
                val title= title_txt.editableText.toString()
                val contents = content_txt.editableText.toString()
                val IsCompleted = iscompleted_check.isChecked

                val todoItem=TodoItem(1, title, contents, IsCompleted, DateCreated="")
                val json= Gson().toJson(todoItem)
                intent.putExtra(TODO_KEY, json)
                setResult(Activity.RESULT_OK,intent)
                finish()
            }
        }
    }
    companion object{
        val TODO_KEY="TODO"
    }
}
