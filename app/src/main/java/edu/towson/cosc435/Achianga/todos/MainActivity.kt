package edu.towson.edu.cosc435.Achianga.todos
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import edu.towson.cosc435.Achianga.todos.Interface.ITodoController
import edu.towson.cosc435.Achianga.todos.NewToDoActivity
import edu.towson.cosc435.Achianga.todos.R
import edu.towson.cosc435.Achianga.todos.TodoEdits
import edu.towson.cosc435.Achianga.todos.models.TodoItem
import edu.towson.edu.cosc435.Achianga.todos4.MainAdapter


import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


@SuppressLint("Registered")
class MainActivity() : AppCompatActivity(), ITodoController {

    private val adapter = MainAdapter(this)

    override lateinit var todos: ITodoRepository
    override lateinit var todosome:ITodoRepository

    override fun lunchAdd() {
        val intent = Intent(this, NewToDoActivity::class.java)
        startActivityForResult(intent, ADD_TODO_REQUEST_CODE)
    }

    override fun launchEdit(idx:Int, todo: TodoItem) {
        val intent = Intent(this, TodoEdits::class.java)
        val todostring = Gson().toJson(todo)
        intent.putExtra("Todo",todostring)
        intent.putExtra("Idx", idx)
        intent.putExtra("Id", todosome.getTodo(idx).id)
        startActivityForResult(intent, EDIT_TODO_REQUEST_CODE)
    }

    override fun getCurrentCount() : Int {
        return todosome.getCount()
    }


    override fun deleteTodo(todo: TodoItem) {

        todosome.remove(todo)
    }

    override fun complete(todo: TodoItem) {
        todosome.IsComplete(todo)
    }

    override fun editTodoItem(todo: TodoItem) {
        todosome.replace(1,todo)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        todosome = ITodoRepository()
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)
        addTodoBTN.setOnClickListener { lunchAdd() }

        val deletedTod:String? = null
        val touchHelper = ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP or ItemTouchHelper.DOWN or  ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT,1){
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                val sourcePosition :Int = viewHolder.adapterPosition
                val targetPosition = target.adapterPosition
                Collections.swap(todosome.getAll(),sourcePosition,targetPosition)
                recyclerView.adapter?.notifyItemMoved(sourcePosition,targetPosition)
                //adapter.notifyItemMoved(sourcePosition,targetPosition)
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {


        }
    })
        touchHelper.attachToRecyclerView(recyclerView)

    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(resultCode){
            Activity.RESULT_OK -> {
                when(requestCode){
                    ADD_TODO_REQUEST_CODE -> {
                        val json = data?.getStringExtra(NewToDoActivity.TODO_KEY)
                        if (json != null){
                            val todo = Gson().fromJson<TodoItem>(json,TodoItem::class.java)
                            todosome.addTodo(todo)
                            recyclerView.adapter?.notifyItemInserted(todosome.getCount())
                        }

                    }
                    EDIT_TODO_REQUEST_CODE -> {
                        val json = data?.getStringExtra(TodoEdits.TODOE_EXTRA_KEY)
                        val idx = data?.getIntExtra(TodoEdits.POSITION_KEY,1)
                        if (json != null && idx != null){
                            val todo = Gson().fromJson<TodoItem>(json,TodoItem::class.java)
                            editTodoItem(todo)
                            recyclerView.adapter?.notifyItemChanged(idx)
                            todosome.addTodo(todo)
                            recyclerView.adapter?.notifyItemInserted(todosome.getCount())
                        }

                    }
                }

            }
            Activity.RESULT_CANCELED -> {
                Toast.makeText(this,"No change",Toast.LENGTH_SHORT).show()
            }
        }

    }

    companion object{
        val ADD_TODO_REQUEST_CODE = 1
        val EDIT_TODO_REQUEST_CODE = 2
    }
}

