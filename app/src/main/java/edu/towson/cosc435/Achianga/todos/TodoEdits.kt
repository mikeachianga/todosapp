package edu.towson.cosc435.Achianga.todos

import android.app.Activity
import android.content.Intent
import android.icu.text.SimpleDateFormat
import android.icu.util.Calendar
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.google.gson.Gson
import edu.towson.cosc435.Achianga.todos.models.TodoItem
import kotlinx.android.synthetic.main.activity_todo_edits.*

class TodoEdits : AppCompatActivity(), View.OnClickListener {

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onClick(view: View?) {
        val titletest = edittitle.text
        val contenttest = editText.text
        if (titletest.isEmpty() || contenttest.isEmpty()){
            Toast.makeText(this,"Missing Title or Content", Toast.LENGTH_SHORT).show()
        }
        else{
            val idx = intent.getIntExtra("Idx",-1)
            val id = intent.getIntExtra("Id",-1)
            val intent = Intent()
            val today = Calendar.getInstance()
            val date = SimpleDateFormat("EEE MMMM d H:m:s Y").format(today.time)
            val title = edittitle.text.toString()
            val text = editText.text.toString()
            val complete = editcheckBox.isChecked

            val todo = TodoItem(id, title, text, complete, date)
            val json = Gson().toJson(todo)
            intent.putExtra(TODOE_EXTRA_KEY, json)
            intent.putExtra(POSITION_KEY, idx)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_todo_edits)


        val json = intent.getStringExtra("Todo")

        val todo = Gson().fromJson<TodoItem>(json, TodoItem::class.java)


        edittitle.setText(todo.title)
        editText.setText(todo.contents)
        editcheckBox.isChecked = todo.IsComplete



        saveedit_btn.setOnClickListener(this)


    }
    companion object{
        val TODOE_EXTRA_KEY = "Todo"
        val POSITION_KEY = "idx"
    }
}
