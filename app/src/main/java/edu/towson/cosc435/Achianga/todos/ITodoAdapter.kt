package edu.towson.edu.cosc435.Achianga.todos4

import android.app.AlertDialog
import android.content.DialogInterface
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import edu.towson.cosc435.Achianga.todos.Interface.ITodoController
import edu.towson.cosc435.Achianga.todos.R
import edu.towson.cosc435.Achianga.todos.models.TodoItem
import kotlinx.android.synthetic.main.todolist.view.*
import kotlinx.android.synthetic.main.todo_view.view.completeview

class MainAdapter (val controller:ITodoController): RecyclerView.Adapter<CustomViewHolder>(){
    override fun getItemCount(): Int {

        return controller.getCurrentCount()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent?.context).inflate(R.layout.todolist, parent, false)
        val viewHolder = CustomViewHolder(layoutInflater)

        layoutInflater.completeview.setOnClickListener {
            val position = viewHolder.adapterPosition
            val todo = controller.todosome.getTodo(position)
            controller.complete(todo)
            this.notifyItemChanged(position)

        }

        layoutInflater.setOnClickListener {
            val position = viewHolder.adapterPosition

            val todo = controller.todosome.getTodo(position)
            val idx = position
            controller.launchEdit(idx, todo)

        }

        layoutInflater.setOnLongClickListener {
            val position = viewHolder.adapterPosition
            val dialogBuilder = AlertDialog.Builder(layoutInflater.context)
            val todo = controller.todosome.getTodo(position)


            dialogBuilder.setMessage("Would you like to delete?")

                .setCancelable(false)

                .setPositiveButton("Proceed", DialogInterface.OnClickListener {
                        dialog, id -> controller.deleteTodo(todo)
                    this.notifyItemRemoved(position)
                })

                .setNegativeButton("Cancel") { dialog, id -> dialog.cancel()
                }


            val alert = dialogBuilder.create()

            alert.setTitle(controller.todosome.getTodo(position).title)

            alert.show()

            return@setOnLongClickListener true
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val todo = controller.todosome.getTodo(position)

        holder.BindTodo(todo)
    }


}

class CustomViewHolder(view: View): RecyclerView.ViewHolder(view) {

    fun BindTodo(todo:TodoItem?){


        if (todo!!.IsComplete) {
            itemView.titleview_txt.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
            itemView.todoview_txt.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
        }
        else{
            itemView.titleview_txt.paintFlags = 0
            itemView.todoview_txt.paintFlags = 0
        }
        //Todo need to make sure its not null
        if (todo.contents.length > 100){
            itemView.todoview_txt.text = todo.contents.subSequence(0,99)
        }
        else{
            itemView.todoview_txt.text = todo.contents
        }

        if (todo.title.length > 100){
            itemView.titleview_txt.text = todo.title.substring(0, 99)
        }
        else{
            itemView.titleview_txt.text = todo.title
        }


        itemView.dateview_txt.text = todo.DateCreated
        itemView.completeview.isChecked = todo.IsComplete
    }

}