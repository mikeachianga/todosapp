package edu.towson.cosc435.Achianga.todos.models

data class TodoItem (
    var id:Int,
    var title:String,
    var contents:String,
    var IsComplete:Boolean,
    var DateCreated:String = "03/09/2020"
)
{
    override fun toString(): String {
        return "\n\n Title :$title\n\n  +  Contents: $contents\n\n  +  complete: $IsComplete\n\n  $DateCreated"
    }
}