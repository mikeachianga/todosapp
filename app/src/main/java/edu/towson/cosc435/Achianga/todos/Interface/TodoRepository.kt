package edu.towson.cosc435.Achianga.todos.Interface

import edu.towson.cosc435.Achianga.todos.models.TodoItem

interface TodoRepository {
    fun getCount(): Int
    fun getTodo(idx: Int): TodoItem
    fun getAll(): List<TodoItem>
    fun remove(todo: TodoItem)
    fun replace(idx: Int, todo: TodoItem)
    fun addTodo(todo: TodoItem)
    fun IsComplete(todo:TodoItem)

}