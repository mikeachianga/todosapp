package edu.towson.cosc435.Achianga.todos.Interface
import edu.towson.cosc435.Achianga.todos.models.TodoItem

interface ITodoController {
    fun deleteTodo(todo: TodoItem)
    fun complete (todo: TodoItem)
    fun lunchAdd()
    fun launchEdit(idx: Int,todo: TodoItem)
    fun editTodoItem(todo: TodoItem)
    fun getCurrentCount() : Int

   // abstract val ADD_TODO_REQUEST_CODE: Int
    //abstract val EDIT_TODO_REQUEST_CODE: Int
    val todos: TodoRepository
    val todosome: TodoRepository

}