package edu.towson.edu.cosc435.Achianga.todos


import android.icu.text.SimpleDateFormat
import android.icu.util.Calendar
import android.os.Build
import androidx.annotation.RequiresApi
import edu.towson.cosc435.Achianga.todos.Interface.TodoRepository
import edu.towson.cosc435.Achianga.todos.models.TodoItem

class ITodoRepository() : TodoRepository{

    private var todos: MutableList<TodoItem> = mutableListOf()


    override fun getCount(): Int {
        return todos.size
    }

    override fun getTodo(idx: Int): TodoItem {
        return todos.get(idx)
    }

    override fun getAll(): List<TodoItem> {
        return todos
    }

    override fun remove(todo: TodoItem) {
        todos.remove(todo)
    }

    override fun replace(idx: Int, todo: TodoItem) {
        if (idx >= todos.size) {
            throw Exception("Out of bounds!")
        }
        todos[idx] = todo
    }

    override fun IsComplete(todo: TodoItem) {
        val check = !todo.IsComplete
        todo.IsComplete = check
        var position = 0
        val id = todo.id

        while (position < todos.size){
            if (todos[position].id == id){
                todos[position].IsComplete = check
                break
            }
            position++
        }
    }

    override fun addTodo(todo: TodoItem) {
        todos.add(todo)
    }



}